export enum Locale {
    'en' = 'en',
    'fr' = 'fr',
    'it' = 'it',
    'hi' = 'hi',
    'ta' = 'ta',
}

export class AppConstants {
    public static readonly config = {
        localeSet: new Set(['en', 'fr', 'it', 'hi', 'ta']),
        defaultLocale: <string>Locale.en,
        imageURL: {
            logo: `assets/strawberry.jpg`
            // logo: `assets/strawberry-seamless-pattern.svg`
            // logo: `assets/logo.png`
        },
    };
}

function getWelcomeMap(): Map<string, string> {
    return <Map<string, string>>new Map()
        .set('en', 'Welcome')
        .set('fr', 'Bienvenu')
        .set('it', 'Benvenuto')
        .set('hi', 'वेलकम')
        .set('ta', 'நல்வரவு');
}

function getStateMap(): Map<string, string> {
    return <Map<string, string>>new Map()
        .set('ca', 'Californian')
        .set('nj', 'New Jerseyans')
        .set('il', 'Illinoisians');
}

function getLocaleMap(): Map<string, IData> {
    const en: IData = { "prodName": "Strawberry Clamshell", "harvestDate": "2020-12-10", "growerName": "Strawberry Ranch", "growerLocation": "Dover, FL, US", "surveyAppUrl": "https://survey-fbdemo2.track-n-trace.net/?traceId=urn:epc:id:sgtin:0715756.054322.1000002001&growerName=Strawberry Ranch&growerLocation=Dover, FL, US&harvestDate=2020-12-10&productName=Strawberry Clamshell", "productUDA": { "nutritionData": { "Carbohydrates": 22.0, "Protein": 0.0, "Fat": 0.2, "Calories": 96.0, "Sugar": 17.2 }, "monthlyRecipe": { "Recipe-1": "Strawberry Rhubarb Pie" } } };
    const fr: IData = { "prodName": "Strawberry Clamshell", "harvestDate": "2020-12-10", "growerName": "Strawberry Ranch", "growerLocation": "Dover, FL, US", "surveyAppUrl": "https://survey-fbdemo2.track-n-trace.net/?traceId=urn:epc:id:sgtin:0715756.054322.1000002001&growerName=Strawberry Ranch&growerLocation=Dover, FL, US&harvestDate=2020-12-10&productName=Strawberry Clamshell", "productUDA": { "nutritionData": { "Carbohydrates": 22.0, "Protein": 0.0, "Fat": 0.2, "Calories": 96.0, "Sugar": 17.2 }, "monthlyRecipe": { "Recipe-1": "Strawberry Rhubarb Pie" } } };
    const it: IData = { "prodName": "Strawberry Clamshell", "harvestDate": "2020-12-10", "growerName": "Strawberry Ranch", "growerLocation": "Dover, FL, US", "surveyAppUrl": "https://survey-fbdemo2.track-n-trace.net/?traceId=urn:epc:id:sgtin:0715756.054322.1000002001&growerName=Strawberry Ranch&growerLocation=Dover, FL, US&harvestDate=2020-12-10&productName=Strawberry Clamshell", "productUDA": { "nutritionData": { "Carbohydrates": 22.0, "Protein": 0.0, "Fat": 0.2, "Calories": 96.0, "Sugar": 17.2 }, "monthlyRecipe": { "Recipe-1": "Strawberry Rhubarb Pie" } } };
    const hi: IData = { "prodName": "Strawberry Clamshell", "harvestDate": "2020-12-10", "growerName": "Strawberry Ranch", "growerLocation": "Dover, FL, US", "surveyAppUrl": "https://survey-fbdemo2.track-n-trace.net/?traceId=urn:epc:id:sgtin:0715756.054322.1000002001&growerName=Strawberry Ranch&growerLocation=Dover, FL, US&harvestDate=2020-12-10&productName=Strawberry Clamshell", "productUDA": { "nutritionData": { "Carbohydrates": 22.0, "Protein": 0.0, "Fat": 0.2, "Calories": 96.0, "Sugar": 17.2 }, "monthlyRecipe": { "Recipe-1": "Strawberry Rhubarb Pie" } } };
    const ta: IData = { "prodName": "Strawberry Clamshell", "harvestDate": "2020-12-10", "growerName": "Strawberry Ranch", "growerLocation": "Dover, FL, US", "surveyAppUrl": "https://survey-fbdemo2.track-n-trace.net/?traceId=urn:epc:id:sgtin:0715756.054322.1000002001&growerName=Strawberry Ranch&growerLocation=Dover, FL, US&harvestDate=2020-12-10&productName=Strawberry Clamshell", "productUDA": { "nutritionData": { "Carbohydrates": 22.0, "Protein": 0.0, "Fat": 0.2, "Calories": 96.0, "Sugar": 17.2 }, "monthlyRecipe": { "Recipe-1": "Strawberry Rhubarb Pie" } } };
    const localeMap: Map<string, IData> = new Map()
        .set(Locale.en, en)
        .set(Locale.fr, fr)
        .set(Locale.it, it)
        .set(Locale.hi, hi)
        .set(Locale.ta, ta);
    return localeMap;
}

function getLabelMap(): Map<string, ILabel> {
    const en: ILabel = { "heading": "Product Information", "tr1": "Nutritional Information", "tr2": "Monthly Recipe", "survey": "Take a Survey", "data": { "prodName": "Product Name", "harvestDate": "Harvest Date", "growerName": "Grower Info", "growerLocation": "Producing Area", "Carbohydrates": "Carbohydrates", "Protein": "Protein", "Fat": "Fat", "Calories": "Calories", "Sugar": "Sugar", "Recipe-1": "Recipe-1" } };
    const fr: ILabel = { "heading": "Information produit", "tr1": "Information nutritionnelle", "tr2": "Recette mensuelle", "survey": "Faire un sondage", "data": { "prodName": "Nom du produit", "harvestDate": "Date de récolte", "growerName": "Informations sur le producteur", "growerLocation": "Zone de production", "Carbohydrates": "Les glucides", "Protein": "Protéine", "Fat": "Gras", "Calories": "Calories", "Sugar": "Sugar", "Recipe-1": "Recipe-1" } };
    const it: ILabel = { "heading": "Informazioni sul prodotto", "tr1": "Informazioni nutrizionali", "tr2": "Ricetta Mensile", "survey": "Fare un sondaggio", "data": { "prodName": "nome del prodotto", "harvestDate": "Data del raccolto", "growerName": "Informazioni sul coltivatore", "growerLocation": "Area di produzione", "Carbohydrates": "Carbohydrates", "Protein": "Proteina", "Fat": "Grasso", "Calories": "Calories", "Sugar": "Sugar", "Recipe-1": "Recipe-1" } };
    const hi: ILabel = { "heading": "उत्पाद की जानकारी", "tr1": "पोषण संबंधी जानकारी", "tr2": "मासिक नुस्खा", "survey": "एक सर्वेक्षण ले", "data": { "prodName": "उत्पाद का नाम", "harvestDate": "फसल की तारीख", "growerName": "उत्पादक जानकारी", "growerLocation": "उत्पादन क्षेत्र", "Carbohydrates": "कार्बोहाइड्रेट", "Protein": "प्रोटीन", "Fat": "मोटी", "Calories": "कैलोरी", "Sugar": "चीनी", "Recipe-1": "विधि-1" } };
    const ta: ILabel = { "heading": "பண்டத்தின் விபரங்கள்", "tr1": "ஊட்டச்சத்து தகவல்", "tr2": "மாதாந்திர செய்முறை", "survey": "ஒரு கணக்கெடுப்பு செய்யவும்", "data": { "prodName": "பொருளின் பெயர்", "harvestDate": "அறுவடை தேதி", "growerName": "வளர்ப்பாளர் தகவல்", "growerLocation": "உற்பத்தி செய்யும் பகுதி", "Carbohydrates": "கார்போஹைட்ரேட்டுகள்", "Protein": "புரோடீன்", "Fat": "கொழுப்பு", "Calories": "கலோரிகள்", "Sugar": "சர்க்கரை", "Recipe-1": "செய்முறை-1" } };
    const localeMap: Map<string, ILabel> = new Map()
        .set(Locale.en, en)
        .set(Locale.fr, fr)
        .set(Locale.it, it)
        .set(Locale.hi, hi)
        .set(Locale.ta, ta);
    return localeMap;
}

export const welcomeMap = getWelcomeMap();
export const stateMap = getStateMap();
export const localeMap = getLocaleMap();
export const labelMap = getLabelMap();

export interface ILabel {
    heading: string;
    tr1: string;
    tr2: string;
    survey: string;
    data: {
        prodName: string;
        harvestDate: string;
        growerName: string;
        growerLocation: string;
        Carbohydrates: string;
        Protein: string;
        Fat: string;
        Calories: string;
        Sugar: string;
        'Recipe-1': string;
    };
}

export interface IData {
    prodName: string;
    harvestDate: string;
    growerName: string;
    growerLocation: string;
    surveyAppUrl: string;
    productUDA: ProductUDA;
}
interface ProductUDA {
    nutritionData: NutritionData;
    monthlyRecipe: MonthlyRecipe;
}
interface NutritionData {
    Carbohydrates: number;
    Protein: number;
    Fat: number;
    Calories: number;
    Sugar: number;
}
interface MonthlyRecipe {
    'Recipe-1': string;
}