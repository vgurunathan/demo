import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, ActivationEnd, Params, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { filter, map } from 'rxjs/operators'
import { AppConstants, IData, ILabel, labelMap, Locale, localeMap, stateMap, welcomeMap } from '../app.constants';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styles: [
  ]
})
export class HomeComponent implements OnInit, OnDestroy {
  config = AppConstants.config;
  localeMap = localeMap;
  stateMap = stateMap;
  labelMap = labelMap;
  welcomeMap = welcomeMap;
  locale: string = this.config.defaultLocale;
  sub$: Subscription | null = null;
  label: ILabel | null = null;
  data: IData | null = null;
  welcome: string = 'Welcome';
  loggedInState: string | null = null;

  constructor(
    private readonly router: Router,
    private readonly route: ActivatedRoute
  ) {
    this.sub$ = this.router.events
      .pipe(
        filter(e => (e instanceof ActivationEnd) && (Object.keys(e.snapshot.params).length > 0)),
        map(e => e instanceof ActivationEnd ? e.snapshot.params : {})
      )
      .subscribe((params: Params) => {
        this.locale = (params[`locale`] && params?.hasOwnProperty('locale') && this.config.localeSet.has(params[`locale`])) ? params[`locale`] : this.config.defaultLocale;
      });
    this.sub$ = this.route.queryParams.subscribe(params => {
      this.loggedInState = null;
      if (params['state'] && this.stateMap.has(params['state'])) {
        this.loggedInState = <string>this.stateMap.get(params['state']);
      }
    }
    );
  }

  ngOnInit(): void {
    this.label = this.getLabel(this.locale);
    this.data = this.getData(this.locale);
    this.welcome = this.welcomeMap.has(this.locale) ? <string>this.welcomeMap.get(this.locale) : 'Welcome';
    console.log(this.locale, this.data);
  }

  getLabel(locale: string): ILabel {
    return (locale && this.labelMap.has(locale)) ? <ILabel>this.labelMap.get(locale) : <ILabel>this.labelMap.get(Locale.en);
  }

  getData(locale: string): IData {
    return (locale && this.localeMap.has(locale)) ? <IData>this.localeMap.get(locale) : <IData>this.localeMap.get(Locale.en);
  }

  ngOnDestroy(): void {
    this.sub$?.unsubscribe();
  }

}
