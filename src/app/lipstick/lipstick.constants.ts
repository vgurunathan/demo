export enum LipstickLocale {
    'en' = 'en',
    'fr' = 'fr',
    'it' = 'it'
}

export class LipstickConstants {
    public static readonly config = {
        localeSet: new Set(['en', 'fr', 'it']),
        defaultLocale: <string>LipstickLocale.en,
        imageURL: {
            logo: `assets/lipstick.jpg`
        },
    };
}

function getDataMap(): Map<string, ILipstick> {
    const en: ILipstick = { "product_title": "L'Oreal Paris Color Riche Moist Matte Lipstick, 266 Pure Rouge, 3.7g", "product_img": "assets/lipstick.jpg", "data": [{ "label": "Age Range (Description)", "value": "Upto 60 years" }, { "label": "Brand", "value": "L'Oreal Paris" }, { "label": "Ingredients", "value": "HYDROXYCITRONELLAL TIN OXIDE MAGNESIUM SILICATE METHYL-2-OCTYNOATE TOCOPHEROL PROPYLENE GLYCOL PROPYL GALLATE CITRIC ACID PARFUM / FRAGRANCE [+/- MAY CONTAIN / PEUT CONTENIR CI 77891 / TITANIUM DIOXIDE CI 45410 / RED 28 LAKE MICA CI 77491, CI 77492, CI 77499 / IRON OXIDES CI 15850 / RED 7 CI 45380 / RED 22 LAKE CI 15985 / YELLOW 6 LAKE CI 77742 / MANGANESE VIOLET CI 19140 / YELLOW 5 LAKE CI 15850 / RED 7 LAKE CI 42090 / BLUE 1 LAKE CI 77120 / BARIUM SULFATE CI 15850 / RED 6 CI 17200 / RED 33 LAKE CI 75470 / CARMINE] FIL CODE B202357/1" }, { "label": "Colour", "value": "266 Pure Rouge" }, { "label": "Finish Type", "value": "Matte" }, { "label": "Skin Tone", "value": "Normal" }, { "label": "Item Form", "value": "Stick" }, { "label": "Sun Protection", "value": "10 SPF" }, { "label": "Item Dimensions\nLxWxH", "value": "20 x 20 x 77 Millimeters" }, { "label": "Item Weight", "value": "24.3 Grams" }] };
    const fr: ILipstick = { "product_title": "L'Oreal Paris Color Riche Moist Matte Lipstick, 266 Pure Rouge, 3.7g", "product_img": "assets/lipstick.jpg", "data": [{ "label": "Tranche d'âge (La description)", "value": "Upto 60 years" }, { "label": "Marque", "value": "L'Oreal Paris" }, { "label": "Ingrédients", "value": "HYDROXYCITRONELLAL TIN OXIDE MAGNESIUM SILICATE METHYL-2-OCTYNOATE TOCOPHEROL PROPYLENE GLYCOL PROPYL GALLATE CITRIC ACID PARFUM / FRAGRANCE [+/- MAY CONTAIN / PEUT CONTENIR CI 77891 / TITANIUM DIOXIDE CI 45410 / RED 28 LAKE MICA CI 77491, CI 77492, CI 77499 / IRON OXIDES CI 15850 / RED 7 CI 45380 / RED 22 LAKE CI 15985 / YELLOW 6 LAKE CI 77742 / MANGANESE VIOLET CI 19140 / YELLOW 5 LAKE CI 15850 / RED 7 LAKE CI 42090 / BLUE 1 LAKE CI 77120 / BARIUM SULFATE CI 15850 / RED 6 CI 17200 / RED 33 LAKE CI 75470 / CARMINE] FIL CODE B202357/1" }, { "label": "Couleur", "value": "266 Pure Rouge" }, { "label": "Type de finition", "value": "Matte" }, { "label": "Teint", "value": "Normal" }, { "label": "Formulaire de l'article", "value": "Stick" }, { "label": "Protection solaire", "value": "10 SPF" }, { "label": "Dimensions de l'article\nLxWxH", "value": "20 x 20 x 77 Millimeters" }, { "label": "Poids de l'article", "value": "24.3 Grams" }] };
    const it: ILipstick = { "product_title": "L'Oreal Paris Color Riche Moist Matte Lipstick, 266 Pure Rouge, 3.7g", "product_img": "assets/lipstick.jpg", "data": [{ "label": "Fascia d'età (Descrizione)", "value": "Upto 60 years" }, { "label": "Marca", "value": "L'Oreal Paris" }, { "label": "ingredienti", "value": "HYDROXYCITRONELLAL TIN OXIDE MAGNESIUM SILICATE METHYL-2-OCTYNOATE TOCOPHEROL PROPYLENE GLYCOL PROPYL GALLATE CITRIC ACID PARFUM / FRAGRANCE [+/- MAY CONTAIN / PEUT CONTENIR CI 77891 / TITANIUM DIOXIDE CI 45410 / RED 28 LAKE MICA CI 77491, CI 77492, CI 77499 / IRON OXIDES CI 15850 / RED 7 CI 45380 / RED 22 LAKE CI 15985 / YELLOW 6 LAKE CI 77742 / MANGANESE VIOLET CI 19140 / YELLOW 5 LAKE CI 15850 / RED 7 LAKE CI 42090 / BLUE 1 LAKE CI 77120 / BARIUM SULFATE CI 15850 / RED 6 CI 17200 / RED 33 LAKE CI 75470 / CARMINE] FIL CODE B202357/1" }, { "label": "Colore", "value": "266 Pure Rouge" }, { "label": "Tipo di finitura", "value": "Matte" }, { "label": "Il tono della pelle", "value": "Normal" }, { "label": "Modulo oggetto", "value": "Stick" }, { "label": "Protezione solare", "value": "10 SPF" }, { "label": "Dimensioni articolo\nLxWxH", "value": "20 x 20 x 77 Millimeters" }, { "label": "Peso dell'oggetto", "value": "24.3 Grams" }] };
    const localeMap: Map<string, ILipstick> = new Map()
        .set(LipstickLocale.en, en)
        .set(LipstickLocale.fr, fr)
        .set(LipstickLocale.it, it);
    return localeMap;
}

export const lipstickLocaleMap = getDataMap();

export interface ILipstick {
    product_title: string;
    product_img: string;
    data: ({
        label: string;
        value: string;
    })[];
};