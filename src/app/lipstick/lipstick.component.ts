import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivationEnd, Router, Params } from '@angular/router';
import { Subscription } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { ILipstick, LipstickConstants, LipstickLocale, lipstickLocaleMap } from './lipstick.constants';

@Component({
  selector: 'app-lipstick',
  templateUrl: './lipstick.component.html',
  styles: [
  ]
})
export class LipstickComponent implements OnInit, OnDestroy {
  config = LipstickConstants.config;
  lipstickLocaleMap = lipstickLocaleMap;
  locale: string = this.config.defaultLocale;
  sub$: Subscription | null = null;
  data: ILipstick | null = null;

  constructor(private router: Router) {
    this.sub$ = this.router.events
      .pipe(
        filter(e => (e instanceof ActivationEnd) && (Object.keys(e.snapshot.params).length > 0)),
        map(e => e instanceof ActivationEnd ? e.snapshot.params : {})
      )
      .subscribe((params: Params) => {
        this.locale = (params[`locale`] && params?.hasOwnProperty('locale') && this.config.localeSet.has(params[`locale`])) ? params[`locale`] : this.config.defaultLocale;
      });
  }

  ngOnInit(): void {
    this.data = this.getData(this.locale);
    console.log(this.locale, this.data);
  }

  getData(locale: string): ILipstick {
    return (locale && this.lipstickLocaleMap.has(locale)) ? <ILipstick>this.lipstickLocaleMap.get(locale) : <ILipstick>this.lipstickLocaleMap.get(LipstickLocale.en);
  }

  get productInfo() { return (this.data && this.data?.data?.length > 0) ? this.data.data : [] }

  ngOnDestroy(): void {
    this.sub$?.unsubscribe();
  }

}
